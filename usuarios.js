class Usuarios{
 constructor(){
     this.personas = [];
     this.status = [];
     this.visto = [];
 }
 //agregarpersonas
 agregarPersonas(id, nick, nomg ,nombre, user, room){
     let per = [];
     per = this.personas.filter(per => per.id === id)[0];
     if (per !== id) {
        let persona = {id, nick, nomg, nombre, user ,room};
        this.personas.push(persona);  
     } else {
        console.log('grupo repetido'); 
     }

    return this.personas;
 }
 agregarClassPerson(id,vi,no,nick,room){
     let per = [];
     per = this.visto.filter(per => per.id === id)[0];
     if (per !== id) {
        let v = {id,vi,no,nick,room}
        this.visto.push(v);
     } else {
         console.log('usuario repetido');
     }

     console.log(this.visto);
 }
 obtlinea(nombre){
     let nom = [];
     nom = this.visto.filter(nom => nom.no === nombre)[0];
     if (nom === undefined){
         return '';
     } else {
         console.log(nom.id);
         return nom.id;
     }
 }
 getroom(room){
    let nom = [];
    nom = this.visto.filter(nom => nom.id === room)[0];
    if (nom === undefined){
        return '';
    } else {
        console.log(nom.room);
        return nom.room;
    }
}
 getNombrepersona(id){
     let persona = [];
     persona = this.personas.filter(persona => persona.id === id)[0];
     if(persona == undefined){
         return '';
     } else {
         return persona.nomg;
     }
     }
 online(id,nombre, status){
    let ini = { id, nombre, status};
        this.status.push(ini);
    return this.status;
 }
 getonline(){
     return this.status;
 }
 getpersonaonline(id){
    let persona = [];
    persona = this.status.filter(persona => persona.id === id)[0];
    if(persona == undefined){
        return '';
    } else {
       return persona;
    } 
 }
 getstatusonline(id){
    let personas =[];
    personas = this.status.filter(persona => persona.nom === id)[0];
    if(personas === undefined)
    {
        return undefined;
    } else{
        //console.log(personas.status);
        return personas.status;
    }
 }
 offline(id){
    let personaborrada = this.getpersonaonline(id);
    console.log(personaborrada);
    this.status = this.status.filter(persona => persona.id != id);
    return personaborrada;
 }
 getPersona(id){
     let persona = [];
     persona = this.personas.filter(persona => persona.id === id)[0];
     if(persona == undefined){
         return '';
     } else {
        return persona;
     }
 }
 getNickPersona(id){
     let persona = [];
     persona = this.personas.filter(persona => persona.nom === id)[0];
     if(persona === undefined){
         return undefined;
     }else{
         console.log(persona.nick);
         return persona.nick;
     }
 }
 getNickGrupos(id){
    let persona = [];
    persona = this.personas.filter(persona => persona.room === id)[0];
    if(persona == undefined){
        return '';
    }else{
        console.log(persona.nick);
        return persona.nick;
    }
}
 getidpersona(id){
     let persona = [];
     persona = this.personas.filter(persona => persona.nombre === id)[0];
     if(persona == undefined){
         return '';
     }else
     {
        //console.log(persona.id);
        return persona.id;
     }
 }
 getuserpersona(id){
     let user = [];
     user = this.personas.filter(persona => persona.user === id)[0];
     if(user == undefined){
         return '';
     }else{
         return user.user;
     }
 }
 getroompersona(id){
    let user = [];
    user = this.personas.filter(persona => persona.id === id)[0];
    if(user == undefined){
        return '';
    }else{
        return user.room;
    }
}
 getusepersona(id){
    let user = [];
    user = this.personas.filter(persona => persona.id === id)[0];
    if(user == undefined){
        return '';
    }else{
        return user.user;
    }
}
 getnompersona(id){
    let user = [];
    user = this.personas.filter(persona => persona.id === id)[0];
    if(user == undefined){
        return '';
    }else{
        return user.nombre;
    }
}
getuserxnompersona(id){
    let user = [];
    user = this.personas.filter(persona => persona.nombe === id)[0];
    if(user == undefined){
        return '';
    }else{
        return user.user;
    }
}
 getPersonas(){
     return this.personas;
 }
 getGruposPersonas(sala){
     let personasalas = this.personas.filter(persona => {persona.room === sala});
     console.log(personasalas);
     return personasalas;   
 }
 eliminarpersona(id){
     let personaborrada = this.getPersona(id);
     console.log(personaborrada);
     this.personas = this.personas.filter(persona => persona.id != id);
     return personaborrada;
    }
    eliminarvisto(id){
        this.visto = this.visto.filter(vist => vist.id != id);
        return this.visto;
    }

}
module.exports = {
 Usuarios   
}