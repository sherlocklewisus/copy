class Notificaciones {
    constructor(){}

    sendNotification(data){
        var headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization":"Basic NmJjYzNiYjEtOWIwYi00MGRhLWIxYjUtZjFlY2QzODgxMGU2"
        };

        var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers
        }
        const https = require('https');
        var req = https.request(options,function(res){
           res.on('data',function(data){
            console.log("Response:");
            console.log(JSON.parse(data));
           }); 
        });
        req.on('error', function(e) {
            console.log("ERROR:");
            console.log(e);
          });
        req.write(JSON.stringify(data));
        req.end();
    }
    notificaciones_grupales(id_usuarios,nom,nick){
        const msg = {
          app_id: "23c2e543-35b6-46c1-a6ed-191bd23620dc",
          contents:{"en":nick,"es":nick},
          android_group:{"en":nom,"es":nom},
          small_icon:"ic_stat_onesignal_default",
          android_accent_color:"0000af",
          filters: [
            {"field": "tag", "key": "id", "relation": "=", "value": id_usuarios},
        ]
        };
        this.sendNotification(msg);
      }
}
module.exports = {
    Notificaciones   
   }