var mongoose = require('mongoose');
const { Schema } = mongoose;
var chatSchema = new Schema({
    nombre_grupo: String,
    nombre: String,
    user: String,
    grupo:String,
    mensaje: String, 
    motivo: String,
    lugar: String,
    personas: String,
    servicio: String,
    total:String,
    idCita: String,
    idServicio: String,
    idReserva: String,
    idPedido: String,
    tipo: String,
    tipo2: String,
    status: String,
    hora: String,
    fecha2: String,
    visto: String,
    img: String,
    video: String,
    audio: String,
    documento: String,
    total2: String,
    nomgrupo: String,
    fecha: {
        type:Date,
        default: Date.now   

    }
});

module.exports = mongoose.model('Chat',chatSchema);

