const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const SocketIO = require('socket.io');
const { Usuarios } = require('./usuarios' );
const { crearMensaje } = require('./mensajes');
const { Notificaciones} = require('./notificaciones');
const usuarios = new Usuarios();
const notification = new Notificaciones();
const http = require('http');
const Chat = require('./chatmodel');
const moment = require('moment')
//init app
let app = express();

let port = process.env.PORT || 9000;
let serve = http.createServer(app);
// let http = require('http').Server(app)

let io = SocketIO(serve);
const escribe = 'en linea';
const all = '';
const nick="";
let obtneruser = '';
let obtnerid = '';
let obtnernom = '';
let obtnernom2 = '';
let obtnergrup = '';
let nombreg = '';
let hold ="";
let user="";
let users="";
let nombre="";
let getnomb="";
var rooms = [];
let usernames = {};
var grupos = [];
let usergrupos = {};
var x ='';
var count='';
var count2='';
//----------------------------------------------------------------------------------------------------------------------------------------------
// Notificaciones
//----------------------------------------------------------------------------------------------------------------------------------------------
io.on('connection',(client) => {
  // io.emit('escribir2',this.escribe);
	//nuevo evento
  client.on('entrarChatPrivado', async (data)=>{
    console.log(data);
    // visto[data.esc] = {id: client.id}
    usuarios.agregarClassPerson(client.id,data.esc,data.nom,data.nick,data.room);
    let esc = '';
    let online = 'En linea';
    let time = moment().format('hh:mm');
    var x = usuarios.obtlinea(data.user);
    var y = usuarios.getroom(x);
    // console.log(data);
    console.log(client.id);
    console.log(x);
    console.log(time);

  // this.escribe = data.esc;
    // io.in(data.room).emit('escribir2',  this.escribe);
    // console.log(visto);
    this.users = usuarios.getusepersona(client.id);
    console.log("entro aqui carlos ");
		if(data.nom != data.user){ //si no es el mismo id
      // se almacena el nombre de usuario en la sesion del socket
      // console.log(client);
      if (data.leer === 1){

      }
      client.username = data.nom;
      //console.log(data.nom);
			// se agrega el nombre de usuario del cliente en variable global}
			usernames[data.nom] = { id:client.id };
			// console.log("nombre"+data);
			if(!rooms.includes(data.room)){

	      client.room = data.room;
				rooms.push(client.room);
        client.join(client.room);
        console.log('diferente');
			}
			else{
        console.log('entro aqui');
				client.room = data.room;
        client.join(client.room);
        if (y !== data.room) {
          console.log('no se actualizan');
        } else {
          var modificados = await Chat.updateMany(
            { $and:
             [
              {$or:[ {nombre: data.nom}, {nombre: data.user} ]},
              {$or:[ {user: data.nom}, {user: data.user} ]}
             ] 
           }, { $set:{'visto':2} });
        }
      }	
      if (x === '') {
        io.in(data.room).emit('escribir2', '');
        
      }else{
        io.in(data.room).emit('escribir4',x);
        io.in(data.room).emit('escribir2', online);
        io.in(data.room).emit('escribir3',data.nombre);
      }
      console.log('al ingresar al chat'+ this.escribe);
      console.log(usernames.id,rooms);
      if(x === '' ){
        console.log("no se actualizo nada");
      }else{
      // var modificados = await Chat.updateMany(
      //    { $and:
      //     [
      //      {$or:[ {nombre: data.nom}, {nombre: data.user} ]},
      //      {$or:[ {user: data.nom}, {user: data.user} ]}
      //     ] 
      //   }, { $set:{'visto':2} });	
      }
      console.log(data.mes);
      var total = await Chat.find(
												{$and:[
																{$or:[ {nombre: data.nom}, {nombre: data.user} ]},
																{$or:[ {user: data.nom}, {user: data.user} ]}
															]
                        }).countDocuments();
                        console.log(total);
                        if (data.mes === undefined) {
                          viejos = await Chat.find(
                            {$and:[
                                    {$or:[ {nombre: data.nom}, {nombre: data.user} ]},
                                    {$or:[ {user: data.nom}, {user: data.user} ]}
                                  ]
                            })
                          .skip(total > 20 ? (total - 20) : 0).sort({ $natural: 1 });
                          this.count = 20;
                          this.count = this.count + 20;
                          io.to(client.id).emit('crearMensaje', viejos);
                          io.in(data.room).emit('crearMensaje',viejos);
                        } else {
                          viejos = await Chat.find(
                            {$and:[
                                    {$or:[ {nombre: data.nom}, {nombre: data.user} ]},
                                    {$or:[ {user: data.nom}, {user: data.user} ]}
                                  ]
                            }).skip(total > this.count ? (total - this.count): 0).sort({$natural:1});
                            this.count = this.count + 20;
                            io.to(client.id).emit('crearMensaje', viejos);
                        }
                      io.to( client.id).emit('mensajes',total);
                      io.to( client.id).emit('chat',1);
                      // io.to(client.id).emit('escribir2', this.escribe );
    }
console.log(data.nick);
    this.nick = data.nick;
  });
  client.on('contar',async(data)=>{

  })
	//nuevo evento
	client.on('enviarMensajePrivado', async (data)=>{
   console.log(data);
    var chat = new Chat({
      nombre: data.nombre,
      user: data.user,
      mensaje: data.mensaje,
      motivo: data.motivo,
      lugar: data.lugar,
      personas: data.personas,
      servicio: data.servicio,
      total: data.total,
      idReserva: data.idReserva,
      idCita: data.idCita,
      idServicio: data.idServicio,
      idPedido: data.idPedido,
      tipo: data.tipo,
      status: data.status,
      img: data.img,
      video: data.video,
      audio: data.audio,
      documento: data.documento,
      hora: data.hora,
      fecha2: data.fecha,
      visto: data.visto,
    });
    await chat.save();
    let esc = '';
    let online = 'En linea';
    let time = moment().format('hh:mm');
    var x = usuarios.obtlinea(data.user);
    var y = usuarios.getroom(x);
    let z = usuarios.getNickPersona(data.user)
    // console.log(data);
    console.log(client.id);
    if (x === '') {
      io.in(data.room).emit('escribir2', '');
    }
    if(x !== ''){
      io.in(data.room).emit('escribir2', online);
      io.in(data.room).emit('escribir3',data.nombre);
    }
    console.log(online);
    console.log(this.escribe);
      if (x!== '' && y === data.room) {
        var modificados = await Chat.updateMany(
          { $and:
           [
            {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
            {$or:[ {user: data.nombre}, {user: data.user} ]}
           ] 
         }, { $set:{'visto':2} });
      } else {
        console.log("no esta en linea");
      }
 		let viejos = [];
		viejos = await Chat.find({
				$and:[
					{$or:[{nombre: data.nombre},{nombre: data.user}]},
					{$or:[{user: data.nombre},{user: data.user}]}
				]
      }).limit(1).sort({ $natural: -1 });
      try {
        let id = usernames[data.user].id
      } catch (error) {
        console.log("indefinido el usuario");
      }
      io.in(data.room).emit('crearMensaje', { from: 'enviarMensajePrivado', data: viejos });
      io.emit('chat',1);
      const nick = z;
      console.log(nick);
      var nicknombre = '';
      
      if (data.tipo === 'imagen'){
         if (data.nickname === undefined) {
          nicknombre = 'Te han enviado una imagen'
        } else {
          nicknombre = data.nickname + ': Imagen';
        }
      }
      if (data.tipo === 'video'){
         if (data.nickname === undefined) {
          nicknombre = 'Te han enviado un video'
        } else {
          nicknombre = data.nickname + ': Video';
        }
      }
      if (data.tipo === 'documento'){
        if (data.nickname=== undefined) {
         nicknombre = 'Te han enviado un documento'
       } else {
         nicknombre = data.nickname + ': Documento';
       }
     }
      if (data.tipo === 'pedido'){
         if (data.nickname === undefined) {
          nicknombre = 'Te han enviado un pedido'
        } else {
          nicknombre = data.nickname + ': Pedido';
        }
      }
      if (data.tipo === 'cita'){
         if (data.nickname === undefined) {
          nicknombre = 'Te han enviado una cita'
        } else {
          nicknombre = data.nickname + ': Cita';
        }
      }
      if (data.tipo === 'reservacion'){
         if (data.nickname === undefined) {
          nicknombre = 'Te han enviado una reservación'
        } else {
          nicknombre = data.nickname + ': Reservación';
        }
      }
      if (data.tipo=== 'servicio'){
         if (data.nickname === undefined) {
          nicknombre = 'Te han enviado un servicio'
        } else {
          nicknombre = data.nickname + ': Servicio';
        }
      }
      if(data.tipo=== 'mensaje'){
    
         if (data.nickname === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre =data.nickname + ": "+ data.mensaje;
        }
      }
      const msg = {
        app_id: "23c2e543-35b6-46c1-a6ed-191bd23620dc",
        contents:{"en":nicknombre,"es":nicknombre},
        android_group:{"en":data.nom,"es":data.nom},
        small_icon:"ic_stat_onesignal_default",
        android_accent_color:"0000af",
        filters: [
          {"field": "tag", "key": "id", "relation": "=", "value": data.user}
      ]
      };
      notification.sendNotification(msg);
  });
  client.on('escribir', async (data)=>{
    let esc = '';
    let online = 'En linea';
    let escri = 'Escribiendo...';
    let time = moment().format('hh:mm');
    var x = usuarios.obtlinea(data.user);
    // console.log(data);
    console.log(client.id);
    console.log(x);
    console.log(time);
    // final = await Chat.updateMany(
    //   {$and:[{nombre:data.user},{user:data.nom}]},
    //   {$set:{'visto':2}}
    // );
    if (data.evento === '' && x === '') {
      io.in(data.room).emit('escribir2', '');
    }
    if(data.evento === '' && x !== ''){
      io.in(data.room).emit('escribir2', online);
    }
    if(data.evento !== '' && x === ''){
      io.in(data.room).emit('escribir2', '');
    }
    if(data.evento !== '' && x!==''){
      io.in(data.room).emit('escribir3',data.nombre);
      io.in(data.room).emit('escribir2', escri); 
    }
    // if(data.evento === ''){
    //     this.escribe = x;
    //     // var x = usuarios.obtlinea(data.user);
    //     io.in(data.room).emit('escribir2', this.escribe);
    //     io.in(data.room).emit('visto',x);
    //   }
    //   if(data.evento !== ''){
    //     let esc =this.escribe + data.nom; 
    //     this.escribe = 'escribiendo...';
    //     // var modificados = await Chat.updateMany( { $and:[ {nombre:data.nombre}, {user:data.user} ] }, { $set:{'visto':2} });
    //     io.in(data.room).emit('escribir3',data.nombre);
    //     io.in(data.room).emit('escribir2', this.escribe);
    //     io.in(data.room).emit('visto',x);
    //   }  
      // }else {
      //   this.escribe = 'escribiendo...';
      //   io.in(data.room).emit('escribir2',  this.escribe  );
      // }
  });
  client.on('entrarChatGrupos', async(data)=>{
    if(data.roome!== undefined){
      if(!grupos.includes(data.room)){

	      client.roome = data.roome;
				grupos.push(client.roome);
        client.join(client.roome);
			}
			else{
				client.room = data.roome;
        client.join(client.roome);
      }	
      console.log(data.mes);
      // client.join(data.roome);	
      usuarios.agregarPersonas(client.id,data.nick,data.nomg,data.nom,data.user,data.roome);
      console.log("chat grupal");
      if (data.mes === undefined) {
        console.log('undefinido');
        var total = await Chat.find({grupo:data.roome}).countDocuments();
        let viejos = await Chat.find({grupo:data.roome}).skip(total > 20 ? (total - 20) : 0).sort({ $natural: 1 });
        io.to(client.id).emit('crearMensaje', viejos);
        this.count2 = 20;
        this.count2 = this.count2 + 20;
      } else {
        console.log('actualizar')
        var total = await Chat.find({grupo:data.roome}).countDocuments();
        let viejos = await Chat.find({grupo:data.roome}).skip(total > this.count2 ? (total - this.count2) : 0).sort({ $natural: 1 });
        io.to(client.id).emit('crearMensaje', viejos);
        this.count2 = this.count2 + 20;
      }
      // var total = await Chat.find({grupo:data.roome}).countDocuments();
      // let viejos = await Chat.find({grupo:data.roome}).skip(total > 20 ? (total - 20) : 0).sort({ $natural: 1 });
      // io.to(client.id).emit('crearMensaje', viejos);
    }
  });
  client.on('mensajeGrupos', async (data)=>{
    // const nombre_p = usuarios.getNickPersona(data.nombre);
    // console.log("Nombre de la persona que lo envia" + nombre_p);
    this.nombreg = usuarios.getNombrepersona(client.id);
    let room = usuarios.getroompersona(client.id);
    console.log(data);
    // console.log("Nombre de la persona que lo envia" + data.getnomb);
    let men = crearMensaje(this.nombreg, data.nombre, data.grupos, data.mensaje, data.tipo, data.video,
      data.img,data.audio,data.documento,data.nombreGrupo);
    var chat = new Chat({
      nombre_grupo: data.getnomb,
      nombre: data.nombre,
      grupo: data.grupos,
      mensaje: data.mensaje,
      tipo: data.tipo,
      audio: data.audio,
      video: data.video,
      img: data.img,
      dcumento:data.documento,
      nomgrupo:data.nombreGrupo
    });
    await chat.save();
    let viejos = [];
    viejos = await Chat.find({grupo: data.grupos});
    console.log("data.grupo"+data.grupos);
    io.in(data.roome).emit('crearMensaje', { from: 'mensajeGrupos', data: viejos });
    io.emit('chat',1);
      // io.to(client.id).emit('crearMensaje', viejos);
      // client.broadcast.to(data.grupos).emit('crearMensaje', viejos);
      // io.to(client.id).emit('crearMensaje', viejos);
      io.in(data.grupos).emit('crearMensaje',viejos);
      let nombregrupos ="";
      if (data.nombreGrupo=== undefined) {
        nombregrupos = 'Tienes un mensaje';
      }else{
        nombregrupos = data.nombreGrupo +' @ '+this.nombreg+' : '+data.mensaje;
      }
    for (let i = 0; i < data.usuarios.length; i++) {
      if(data.nombre !== data.usuarios[i]){
        notification.notificaciones_grupales(data.usuarios[i],data.nom,nombregrupos);
      }
    }
  });
  client.on('open', async (data)=>{
    usuarios.online(client.id,data.nom,data.status);
    this.escribe = data.status;
    let p = usuarios.getonline();
  });
  client.on('disconnect',()=>{
    // this.all = this.offline;
    // client.broadcast.to(this.obtnerid).emit('escribir2', new Date().getTime());
    let oni = usuarios.offline(client.id);
    console.log(client.id);
    let p = usuarios.getonline();
    // io.emit('escribir2',"");
    this.escribe= "";
    let personaeliminada = usuarios.eliminarpersona(client.id);
    usuarios.eliminarvisto(client.id);
    io.emit('listaPersonas',usuarios.getPersonas());
  });
  // client.on('notificar', async (data)=>{
  //   let viejos = await Chat.find(
  //     {$and:[{nombre: data.nom},{user: data.user}]}
  //     );
  //   let viejos2 = await Chat.aggregate([{
  //     $group:{
  //        _id:{$and:[{nombre: data.nom},{user: data.user}]},
  //        total2: {$sum:"$visto"},
  //        visto:{ $sum:'1'}
  //            },
  //   }]);
  //     io.to(client.id).emit('old', viejos);
  // });
  client.on('cita', async (data)=>{
    console.log(data)
    this.obtnerid = usuarios.getidpersona(data.user);
    let cita = await Chat.findOneAndUpdate(
      {'idCita':data.idCita},
      {$set:{'status': data.status }},
      {new: true},
      async (err, doc) =>{
        if (err) {
          console.log("Something wrong when updating data!");
      } else{
        var total = await Chat.find(
          {$and:[
                  {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                  {$or:[ {user: data.nombre}, {user: data.user} ]}
                ]
          }).countDocuments();
        let viejos = [];
        viejos = await Chat.find(
          {$and:[
                  {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                  {$or:[ {user: data.nombre}, {user: data.user} ]}
                ]
          })
        .skip(total > 50 ? (total - 50) : 0).sort({ $natural: 1 });
          try {
            const id = usernames[data.user].id;
            console.log(id);
          } catch (error) {
            console.log("indefinido el usuario");
          }
          console.log(viejos);
          let con = data.nombre;
          // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
          io.to(data.room).emit('crearMensaje', viejos);
      }
      });
      
      // io.to(client.id).emit('crearMensaje', viejos);
      // client.broadcast.to(this.obtnerid).emit('crearMensaje', viejos);
  });
  client.on('modificar_cita',async (data)=>{
    this.obtnerid = usuarios.getidpersona(data.user);
    console.log(data);
    let modificar_cita = await Chat.findOneAndUpdate(
      {'idCita':data.idCita},
      {$set:{'motivo': data.motivo,'tipo2':data.tipo,'fecha2':data.fecha,'hora':data.hora, 'visto':data.visto}},
      {new: true},
      async (err, doc) =>{
        if(err){
          console.log('error a actualizar la cita');
        } else{
          var chat = new Chat({
            nombre: data.nombre,
            user: data.user,
            idCita: data.idCita,
            tipo2: data.tipo2,
            visto:data.visto,
            mensaje: 'Se modificado una cita',
          });
          await chat.save();
          final = await Chat.updateMany(
            {$and:[{nombre:data.user},{user:data.nombre}]},
            {$set:{'visto':2}}
          );
          var total = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            }).countDocuments();
          let viejos = [];
          viejos = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            })
          .skip(total > 50 ? (total - 50) : 0).sort({ $natural: 1 });
            try {
              const id = usernames[data.user].id;
              console.log(id);
            } catch (error) {
              console.log("indefinido el usuario");
            }
            console.log(viejos);
            let con = data.nombre;
            // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
            io.to(data.room).emit('crearMensaje', viejos);
            // io.in(data.room).emit('crearMensaje',  viejos );
            // client.broadcast.to(client.id).emit('crearMensaje',  viejos);
            // io.to(client.id).emit('crearMensaje', viejos);
            // io.to(id).emit('crearMensaje',viejos);
            // io.in(data.room).emit('crearMensaje',  viejos);
              }
      }
    );
  });
  client.on('pedido',async (data)=>{
    console.log(data,'Hola hehe');
    this.obtnerid = usuarios.getidpersona(data.user);
    let pedido = await Chat.findOneAndUpdate(
      {'idPedido':data.idPedido},
      {$set:{'status':data.status}},
      {new: true},
       async (err,doc)=>{
        if(err){
          console.log("Something wrong when updating data!");
        } else{
          var total = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            }).countDocuments();
          let viejos = [];
          viejos = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            })
          .skip(total > 50 ? (total - 50) : 0).sort({ $natural: 1 });
            try {
              const id = usernames[data.user].id;
              console.log(id);
            } catch (error) {
              console.log("indefinido el usuario");
            }
            console.log(viejos);
            let con = data.nombre;
            // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
            io.to(data.room).emit('crearMensaje', viejos);
        }
      });
      console.log(pedido);
  });
  client.on('modificar_pedido', async (data)=>{
    let p = usuarios.getidpersona(data.user);
    let modificar_pedido = await Chat.findOneAndUpdate(
      {'idPedido': data.idPedido},
      {$set:{'total': data.total}},
      {new: true},
      async(err,doc)=>{
        if(err){
          console.log('error en el pedido');
        } else{
          var chat = new Chat({
            nombre: data.nombre,
            user: data.user,
            idPedido: data.idPedido,
            tipo2: data.tipo2,
            visto:data.visto,
            mensaje: 'Se modificado el pedio',
          });
          await chat.save();
          final = await Chat.updateMany(
            {$and:[{nombre:data.user},{user:data.nombre}]},
            {$set:{'visto':2}}
          );
          var total = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            }).countDocuments();
          let viejos = [];
          viejos = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            })
          .skip(total > 50 ? (total - 50) : 0).sort({ $natural: 1 });
            try {
              const id = usernames[data.user].id;
              console.log(id);
            } catch (error) {
              console.log("indefinido el usuario");
            }
            console.log(viejos);
            let con = data.nombre;
            // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
            io.to(data.room).emit('crearMensaje', viejos);
        }
      });
  });
  client.on('reservacion', async (data)=>{
    console.log(data);
    let cita = await Chat.findOneAndUpdate(
      {'idReserva':data.idReserva},
      {$set:{'status': data.status}},
      {new: true},
      async (err, doc) =>{
        if (err) {
          console.log("Something wrong when updating data!");
      } else{
        
        var total = await Chat.find(
          {$and:[
                  {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                  {$or:[ {user: data.nombre}, {user: data.user} ]}
                ]
          }).countDocuments();
        let viejos = [];
        viejos = await Chat.find(
          {$and:[
                  {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                  {$or:[ {user: data.nombre}, {user: data.user} ]}
                ]
          })
        .skip(total > 50 ? (total - 50) : 0).sort({ $natural: 1 });
          try {
            const id = usernames[data.user].id;
            console.log(id);
          } catch (error) {
            console.log("indefinido el usuario");
          }
          console.log(viejos);
          let con = data.nombre;
          // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
          io.to(data.room).emit('crearMensaje', viejos);
      }
  });
});
  client.on('modificar_reservacion',async (data)=>{
    console.log(data);
    this.obtnerid = usuarios.getidpersona(data.user);
    let modificar_reservacion = await Chat.findOneAndUpdate(
      {'idReserva':data.idReserva},
      {$set:{'motivo':data.motivo, 'fecha2': data.fecha,
      'hora': data.hora, 'personas': data.personas,
      'visto':data.visto,'tipo2':data.tipo}},
      {new:true},
      async (err, doc)=> {
        if(err){
          console.log('error al actualizar una reservación');
        } else{
          var chat = new Chat({
            nombre: data.nombre,
            user: data.user,
            idReserva: data.idReserva,
            tipo2: data.tipo2,
            visto:data.visto,
            mensaje: 'Se modificado una reservación',
          });
          await chat.save();
          final = await Chat.updateMany(
            {$and:[{nombre:data.user},{user:data.nombre}]},
            {$set:{'visto':2}}
          );
          var total = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            }).countDocuments();
          let viejos = [];
          viejos = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            })
          .skip(total > 50 ? (total - 50) : 0).sort({ $natural: 1 });
            try {
              const id = usernames[data.user].id;
              console.log(id);
            } catch (error) {
              console.log("indefinido el usuario");
            }
            console.log(viejos);
            let con = data.nombre;
            // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
            io.to(data.room).emit('crearMensaje', viejos);
        }
        });
  });
  client.on('servicio', async (data)=>{
    console.log(data);
    let x = usuarios.getidpersona(data.user);
    let cita = await Chat.findOneAndUpdate(
      {'idServicio':data.idServicio},
      {$set:{'status': data.status }},
      {new: true},
      async(err, doc) =>{
        if (err) {
          console.log("Something wrong when updating data!");
      }else{
        
        var total = await Chat.find(
          {$and:[
                  {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                  {$or:[ {user: data.nombre}, {user: data.user} ]}
                ]
          }).countDocuments();
        let viejos = [];
        viejos = await Chat.find(
          {$and:[
                  {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                  {$or:[ {user: data.nombre}, {user: data.user} ]}
                ]
          })
        .skip(total > 50 ? (total - 50) : 0).sort({ $natural: 1 });
          try {
            const id = usernames[data.user].id;
            console.log(id);
          } catch (error) {
            console.log("indefinido el usuario");
          }
          console.log(viejos);
          let con = data.nombre;
          // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
          io.to(data.room).emit('crearMensaje', viejos);
      }
      });
  });
  client.on('modificar_servicio',async (data)=>{
    this.obtnerid = usuarios.getidpersona(data.user);
    let modificar_servicio = await Chat.findOneAndUpdate(
      {'idServicio': data.idServicio},
      {$set:{'motivo': data.motivo, 'fecha2':data.fecha,
      'hora':data.hora,'lugar':data.lugar, 'visto': data.visto,  'tipo2':data.tipo}},
      {new: true},
      async (err,doc)=>{
        if(err){
          console.log('error a actualizar el servicio');
        }else{
          var chat = new Chat({
            nombre: data.nombre,
            user: data.user,
            idServicio: data.idServicio,
            tipo2: data.tipo2,
            visto:data.visto,
            mensaje: 'Se modificado el servicio',
          });
          await chat.save();
          final = await Chat.updateMany(
            {$and:[{nombre:data.user},{user:data.nombre}]},
            {$set:{'visto':2}}
          );
          var total = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            }).countDocuments();
          let viejos = [];
          viejos = await Chat.find(
            {$and:[
                    {$or:[ {nombre: data.nombre}, {nombre: data.user} ]},
                    {$or:[ {user: data.nombre}, {user: data.user} ]}
                  ]
            })
          .skip(total >50 ? (total - 50) : 0).sort({ $natural: 1 });
            try {
              const id = usernames[data.user].id;
              console.log(id);
            } catch (error) {
              console.log("indefinido el usuario");
            }
            console.log(viejos);
            let con = data.nombre;
            // client.broadcast.to(data.room).emit('crearMensaje',  viejos);
            io.to(data.room).emit('crearMensaje', viejos);
        }
      }
      );

  });
  client.on('mensajePrivado',async (data)=>{
		console.log("mensajePrivado");
    this.nombre = data.nombre;
    this.user = data.user;
    let mensaje = crearMensaje(
      data.nombre,data.user,data.mensaje,data.motivo,
      data.lugar,data.personas,data.servicio,data.hora,
      data.fecha,data.total,data.idCita,data.idReserva,data.idServicio,
      data.idPedido,data.tipo, data.status,data.img,data.video,data.audio, data.documento, data.visto);
    var chat = new Chat({
      nombre: data.nombre,
      user: data.user,
      mensaje: data.mensaje,
      motivo: data.motivo,
      lugar: data.lugar,
      personas: data.personas,
      servicio: data.servicio,
      total: data.total,
      idReserva: data.idReserva,
      idCita: data.idCita,
      idServicio: data.idServicio,
      idPedido: data.idPedido,
      tipo: data.tipo,
      status: data.status,
      img: data.img,
      video: data.video,
      audio: data.audio,
      documento: data.documento,
      hora: data.hora,
      fecha2: data.fecha,
      visto: data.visto,
    });
    await chat.save();
    let id = usuarios.getidpersona(data.user);
    let x = usuarios.getusepersona(this.obtnerid);
    this.obtnernom2 = usuarios.getnompersona(client.id);
    let z = usuarios.getnompersona(id);

    let con = data.nombre;
    let x2 = 'en linea';
    this.escribe = x2;
    console.log("persona 1 "+ this.obtnernom,"persona 2 "+data.user);
    console.log("persona 2 "+x,"persona 1 "+ data.nombre);
    console.log("persona 3 "+this.obtnernom2,"persona 4" + z);
   // console.log( "usuario"+data.user,"nombre"+data.nombre);
    // this.obtnerid = usuarios.getidpersona(data.user);
    if(this.obtnernom === data.nombre && x === data.user){
      console.log( data.nombre, data.user);
      console.log("entro aqui");
      final = await Chat.updateMany(
        {$and:[{nombre:data.nombre},{user:data.user}]},
        {$set:{'visto':2}}
      );
      viejos = await Chat.find({$and:[
        {
        $or:[{nombre: data.nombre},{nombre: data.user}]
        },
        {
        $or:[{user: data.nombre},{user: data.user}]
        }]}).limit(50).sort({ $natural: -1 });
      //.limit(10).sort({ $natural: -1 })
			console.log("crearMensaje 1");
			io.to(client.id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos });
      client.broadcast.to(id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
      const nick =  usuarios.getNickPersona(con);
  console.log(nick);
  var nicknombre = '';
  if (data.tipo === 'imagen'){
     if (nick === undefined) {
      nicknombre = 'Te han enviado una imagen'
    } else {
      nicknombre = nick + ': Imagen';
    }
  }
  if (data.tipo === 'video'){
     if (nick === undefined) {
      nicknombre = 'Te han enviado un video'
    } else {
      nicknombre = nick + ': Video';
    }
  }
  if (data.tipo === 'documento'){
    if (nick === undefined) {
     nicknombre = 'Te han enviado un documento'
   } else {
     nicknombre = nick + ': Documento';
   }
 }
  if (data.tipo === 'pedido'){
     if (nick === undefined) {
      nicknombre = 'Te han enviado un pedido'
    } else {
      nicknombre = nick + ': Pedido';
    }
  }
  if (data.tipo === 'cita'){
     if (nick === undefined) {
      nicknombre = 'Te han enviado una cita'
    } else {
      nicknombre = nick + ': Cita';
    }
  }
  if (data.tipo === 'reservacion'){
     if (nick === undefined) {
      nicknombre = 'Te han enviado una reservación'
    } else {
      nicknombre = nick + ': Reservación';
    }
  }
  if (data.tipo=== 'servicio'){
     if (nick === undefined) {
      nicknombre = 'Te han enviado un servicio'
    } else {
      nicknombre = nick + ': Servicio';
    }
  }
  if(data.tipo=== 'mensaje'){

     if (nick === undefined) {
      nicknombre = 'Grupos'
    } else {
      nicknombre =nick + ": "+ data.mensaje;
    }
  }
  const msg = {
    app_id: "23c2e543-35b6-46c1-a6ed-191bd23620dc",
    contents:{"en":nicknombre,"es":nicknombre},
    // android_group:{"en":data.nom,"es":data.nom},
    small_icon:"ic_stat_onesignal_default",
    android_accent_color:"0000af",
    filters: [
	  	{"field": "tag", "key": "id", "relation": "=", "value": data.user}
	]
  };
    notification.sendNotification(msg);
      
    }
		else if(data.nombre === this.obtnernom  && this.user === x){
      final = await Chat.updateMany(
        {$and:[{nombre:data.nombre},{user:data.user}]},
        {$set:{'visto':2}}
      );
       viejos = await Chat.find({$and:[
        {
        $or:[{nombre: data.nombre},{nombre: data.user}]
        },
        {
        $or:[{user: data.nombre},{user: data.user}]
        }]});
      console.log("crearMensaje 2");
			io.to(client.id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
      client.broadcast.to(id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
      const nick =  usuarios.getNickPersona(con);
      console.log(nick);
      var nicknombre = '';
      if (data.tipo === 'imagen'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado una imagen'
        } else {
          nicknombre = nick + ': Imagen';
        }
      }
      if (data.tipo === 'video'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado un video'
        } else {
          nicknombre = nick + ': Video';
        }
      }
      if (data.tipo === 'documento'){
        if (nick === undefined) {
         nicknombre = 'Te han enviado un documento'
       } else {
         nicknombre = nick + ': Documento';
       }
     }
      if (data.tipo === 'pedido'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado un pedido'
        } else {
          nicknombre = nick + ': Pedido';
        }
      }
      if (data.tipo === 'cita'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado una cita'
        } else {
          nicknombre = nick + ': Cita';
        }
      }
      if (data.tipo === 'reservacion'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado una reservación'
        } else {
          nicknombre = nick + ': Reservación';
        }
      }
      if (data.tipo=== 'servicio'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado un servicio'
        } else {
          nicknombre = nick + ': Servicio';
        }
      }
      if(data.tipo=== 'mensaje'){
    
         if (nick === undefined) {
          nicknombre = 'Grupos'
        } else {
          nicknombre =nick + ": "+ data.mensaje;
        }
      }
      const msg = {
        app_id: "23c2e543-35b6-46c1-a6ed-191bd23620dc",
        contents:{"en":nicknombre,"es":nicknombre},
        // android_group:{"en":data.nom,"es":data.nom},
        small_icon:"ic_stat_onesignal_default",
        android_accent_color:"0000af",
        filters: [
          {"field": "tag", "key": "id", "relation": "=", "value": data.user}
      ]
      };
        notification.sendNotification(msg);
     
    }
    else {
      viejos = await Chat.find({$and:[
       {
       $or:[{nombre: data.nombre},{nombre: data.user}]
       },
       {
       $or:[{user: data.nombre},{user: data.user}]
       }]}).limit(1).sort({ $natural: -1 });
			console.log("crearMensaje 3");
     io.to(client.id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
   }
    if(this.obtnernom2 === data.nombre && z === this.nombre){
      final = await Chat.updateMany(
        {$and:[{nombre:data.nombre},{user:data.user}]},
        {$set:{'visto':2}}
      );
      viejos = await Chat.find({$and:[
        {
        $or:[{nombre: data.nombre},{nombre: data.user}]
        },
        {
        $or:[{user: data.nombre},{user: data.user}]
        }]});
      console.log("crearMensaje 4");
			io.to(client.id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
      client.broadcast.to(id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
      const nick =  usuarios.getNickPersona(con);
      console.log(nick);
      var nicknombre = '';
      if (data.tipo === 'imagen'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado una Imagen'
        } else {
          nicknombre = nick + ': Imagen';
        }
      }
      if (data.tipo === 'video'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado un video'
        } else {
          nicknombre = nick + ': Video';
        }
      }
      if (data.tipo === 'documento'){
        if (nick === undefined) {
         nicknombre = 'Tienes un mensaje'
       } else {
         nicknombre = nick + ': Documento';
       }
     }
      if (data.tipo === 'pedido'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Pedido';
        }
      }
      if (data.tipo === 'cita'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Cita';
        }
      }
      if (data.tipo === 'reservacion'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado un reservación'
        } else {
          nicknombre = nick + ': Reservación';
        }
      }
      if (data.tipo=== 'servicio'){
         if (nick === undefined) {
          nicknombre = 'Te han enviado un servicio'
        } else {
          nicknombre = nick + ': Servicio';
        }
      }
      if(data.tipo=== 'mensaje'){
    
         if (nick === undefined) {
          nicknombre = 'Grupos'
        } else {
          nicknombre =nick + ": "+ data.mensaje;
        }
      }
      const msg = {
        app_id: "23c2e543-35b6-46c1-a6ed-191bd23620dc",
        contents:{"en":nicknombre,"es":nicknombre},
        // android_group:{"en":data.nom,"es":data.nom},
        small_icon:"ic_stat_onesignal_default",
        android_accent_color:"0000af",
        filters: [
          {"field": "tag", "key": "id", "relation": "=", "value": data.user}
      ]
      };
        notification.sendNotification(msg);
     
    }
		else if(data.nombre === this.obtnernom2 && z === this.user){
      final = await Chat.updateMany(
        {$and:[{nombre:data.nombre},{user:data.user}]},
        {$set:{'visto':2}}
      );
       viejos = await Chat.find({$and:[
        {
        $or:[{nombre: data.nombre},{nombre: data.user}]
        },
        {
        $or:[{user: data.nombre},{user: data.user}]
        }]}).limit(1).sort({ $natural: -1 });
      console.log("crearMensaje 5");
			io.to(client.id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
      client.broadcast.to(id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
      const nick =  usuarios.getNickPersona(con);
      console.log(nick);
      var nicknombre = '';
      if (data.tipo === 'imagen'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Imagen';
        }
      }
      if (data.tipo === 'video'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Video';
        }
      }
      if (data.tipo === 'documento'){
        if (nick === undefined) {
         nicknombre = 'Tienes un mensaje'
       } else {
         nicknombre = nick + ': Documento';
       }
     }
      if (data.tipo === 'pedido'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Pedido';
        }
      }
      if (data.tipo === 'cita'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Cita';
        }
      }
      if (data.tipo === 'reservacion'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Reservación';
        }
      }
      if (data.tipo=== 'servicio'){
         if (nick === undefined) {
          nicknombre = 'Tienes un mensaje'
        } else {
          nicknombre = nick + ': Servicio';
        }
      }
      if(data.tipo=== 'mensaje'){
    
         if (nick === undefined) {
          nicknombre = 'Grupos'
        } else {
          nicknombre =nick + ": "+ data.mensaje;
        }
      }
      const msg = {
        app_id: "23c2e543-35b6-46c1-a6ed-191bd23620dc",
        contents:{"en":nicknombre,"es":nicknombre},
        // android_group:{"en":data.nom,"es":data.nom},
        small_icon:"ic_stat_onesignal_default",
        android_accent_color:"0000af",
        filters: [
          {"field": "tag", "key": "id", "relation": "=", "value": data.user}
      ]
      };
        notification.sendNotification(msg);
    }
    else {
       viejos = await Chat.find({$and:[
        {
        $or:[{nombre: data.nombre},{nombre: data.user}]
        },
        {
        $or:[{user: data.nombre},{user: data.user}]
        }]}).limit(1).sort({ $natural: -1 });
			console.log("crearMensaje 6");
      io.to(client.id).emit('crearMensaje', { from: 'mensajePrivado', data: viejos } );
    }
      console.log("escribir2 1");
			io.to(client.id).emit('escribir2', this.escribe );
      client.broadcast.to(id).emit('escribir2', this.escribe );
    });
});
let api = require("./routing");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect('mongodb://admin_orvity:Orvity2019@ds351987.mlab.com:51987/heroku_9vdhszb5', { useNewUrlParser: true })
.then( db => console.log('Conectado a mongoDB'))
.catch(err => console.log(err));

app.use('/api',api);
app.get('/', function(req,res){
res.send('Hello word');
res.setHeader('Access-Control-Allow-Origin', '*');
res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
res.setHeader('Access-Control-Allow-Credentials', true); // If needed
res.send('cors problem fixed:)');
});
//servidor web
serve.listen(port,()=> {
    console.log("Running serve on port " + port);
});
